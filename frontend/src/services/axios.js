import axios from 'axios'
export const createAxiosInstance = socket => {
  const instance = axios.create({})
  instance.interceptors.response.use(
    function(response) {
      socket.emit('session')
      return response
    },
    function(error) {
      // Do something with response error
      return Promise.reject(error)
    }
  )

  return instance
}
