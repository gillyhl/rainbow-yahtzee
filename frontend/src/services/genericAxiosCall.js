export default axiosCall =>
  axiosCall
    .then(res => ({ result: res.data }))
    .catch(e => ({ error: e.response.data }))
