import { createAxiosInstance } from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'
import socket from 'socket'

const axios = createAxiosInstance(socket)

export const createGame = name =>
  genericAxiosCall(
    axios.post('/api/v1/game', {
      name
    })
  )

export const joinGame = (name, code) =>
  genericAxiosCall(
    axios.patch('/api/v1/game/join', {
      name,
      code
    })
  )

export const getGame = () => genericAxiosCall(axios.get('/api/v1/game'))

export const deleteGame = () => genericAxiosCall(axios.delete('/api/v1/game'))

export const leaveGame = () =>
  genericAxiosCall(axios.patch('/api/v1/game/leave'))

export const startGame = () =>
  genericAxiosCall(axios.patch('/api/v1/game/start'))

export const rollDice = dice =>
  genericAxiosCall(axios.patch('/api/v1/game/roll', { dice }))

export const scoreRound = (field = 'THREE_TIMES') =>
  genericAxiosCall(axios.patch('/api/v1/game/score', { field }))
