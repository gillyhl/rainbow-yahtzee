import io from 'socket.io-client'
const socket = io()
socket.on('connect', () => {})

export const subscribeToRefresh = cb => {
  socket.on('refresh', (data) => {
    cb(data)
  })
}

export const subscribeToDeletedGame = cb => {
  socket.on('deleted', () => {
    cb()
  })
}

export const reconnect = () => {
  socket.disconnect()
  socket.connect()
}

export default socket
