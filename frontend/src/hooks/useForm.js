import { useState } from 'react'
import _ from 'lodash'

const getFormMetaFromFormValues = formValues =>
  _.mapValues(formValues, v => ({
    value: v.value,
    onChange: v.onChange,
    meta: {
      dirty: false
    }
  }))

const formMetaFactory = (formValues, validators) => {
  const formMetaFromValues = getFormMetaFromFormValues(formValues)
  return _.mapValues(formMetaFromValues, (v, k) => ({
    ...v,
    validations: validators[k] || []
  }))
}

// USE MEMO/EFFECT?
export default (formValues = {}, validators = {}, handleSubmit = () => {}) => {
  const [formMeta, setFormMeta] = useState(
    formMetaFactory(formValues, validators)
  )

  const values = _.mapValues(formMeta, v => v.value)

  const errors = _.mapValues(formMeta, v =>
    v.validations
      .filter(validation => !validation.isValid(values))
      .map(validation => validation.message)
  )

  const isValidForm = !_.chain(errors)
    .values()
    .flatten()
    .value().length

  const isValidField = field => !errors[field].length

  const formData = _.mapValues(formMeta, (v, k) => ({
    ...v,
    value: v.value,
    onChange: e => {
      const newValue = v.onChange(e)
      setFormMeta({
        ...formMeta,
        [k]: {
          ...formMeta[k],
          value: newValue,
          meta: {
            ...formMeta[k].meta,
            dirty: true
          }
        }
      })
    },
    onBlur: () => {
      setFormMeta({
        ...formMeta,
        [k]: {
          ...formMeta[k],
          meta: {
            ...formMeta[k].meta,
            touched: true
          }
        }
      })
    }
  }))

  const isTouched = field => formMeta[field] && formMeta[field].meta.touched
  const isDirty = field => formMeta[field] && formMeta[field].meta.dirty
  const isTouchedOrDirty = field => isTouched(field) || isDirty(field)
  const submit = e => {
    e.preventDefault()
    setFormMeta(
      _.mapValues(formMeta, v => ({
        ...v,
        meta: {
          ...v.meta,
          touched: true,
          dirty: true
        }
      }))
    )
    handleSubmit(isValidForm, values)
  }

  return [
    formData,
    errors,
    submit,
    values,
    {
      isValidField,
      isTouched,
      isDirty,
      isTouchedOrDirty
    }
  ]
}
