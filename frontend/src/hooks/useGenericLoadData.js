import { useEffect } from 'react'

const defaultFunction = () => {}
/**
 * Service method should be an async function that returns an object with keys:
 *  result: the result value
 *  error: the error response value
 */
export default (
  serviceMethod,
  watchers = [],
  setData = defaultFunction,
  setError = defaultFunction,
  setLoading = defaultFunction
) => {
  useEffect(() => {
    fetchData()
  }, watchers)

  const fetchData = async () => {
    setLoading(true)
    try {
      const { result, error } = await serviceMethod()
      if (result) setData(result.data)
      if (error) setError(error.data)
    } catch (e) {
    } finally {
      setLoading(false)
    }
  }
}
