import React, { useContext } from 'react'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Button from '@material-ui/core/Button'
import SCORING from 'constants/scoring'
import { getIsYourTurn } from '../service'
import { scoreRound } from 'services/games'

import { withStyles } from '@material-ui/core/styles'
import GameContext from 'context/GameContext'
import ErrorContext from 'context/ErrorContext'
import sum from 'lodash/sum'

const styles = theme => ({
  scoringCell: {
    textAlign: 'center'
  },
  button: {
    marginTop: theme.spacing.unit
  }
})
const ScoringTable = ({ scoringFields, classes, isUpperBonus }) => {
  const {
    game: { players, sessionId, currentTurn, scores },
    setGame,
    setGameLoading
  } = useContext(GameContext)
  const { displayErrorMessage } = useContext(ErrorContext)
  const currentPlayer = players.find(
    player => player.sessionId === currentTurn.playerSessionId
  )
  return (
    <Table>
      <colgroup>
        <col width="50%" />
        <col width="50%" />
      </colgroup>
      <TableHead>
        <TableRow>
          <TableCell />
          <TableCell className={classes.scoringCell}>
            {currentPlayer.name}
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {scoringFields.map(k => {
          const score = scores[currentPlayer.sessionId].meta[k]
          const possibleScore = SCORING[k].score
            ? SCORING[k].score(currentTurn.dice)
            : 0
          const isYourTurn = getIsYourTurn({ currentTurn, sessionId })
          const onScoreButtonClick = async () => {
            if (!isYourTurn) return
            setGameLoading(true)
            try {
              const { error } = await scoreRound(k)
              if (error) return displayErrorMessage(error.data.message)
            } finally {
              setGameLoading(false)
            }
          }
          const display =
            score === null ? (
              currentPlayer.sessionId === currentTurn.playerSessionId ? (
                <Button
                  variant={isYourTurn ? 'contained' : 'text'}
                  color={isYourTurn ? 'primary' : 'default'}
                  onClick={onScoreButtonClick}
                  size="small">
                  {possibleScore}
                </Button>
              ) : (
                <Button variant="text" size="small">
                  -
                </Button>
              )
            ) : (
              <Button variant="text" size="small">
                {score}
              </Button>
            )
          return (
            <TableRow key={k}>
              <TableCell>{k}</TableCell>

              <TableCell className={classes.scoringCell}>{display}</TableCell>
            </TableRow>
          )
        })}
        {isUpperBonus && (
          <TableRow>
            <TableCell>UPPER_TOTAL</TableCell>

            <TableCell className={classes.scoringCell}>
              <Button variant="text" size="small">
                {sum(
                  scoringFields
                    .map(k => scores[currentPlayer.sessionId].meta[k])
                    .filter(Boolean)
                )}
              </Button>
            </TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  )
}

export default withStyles(styles)(ScoringTable)
