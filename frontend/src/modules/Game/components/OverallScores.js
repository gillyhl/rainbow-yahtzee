import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import GenericGrid from 'components/GenericGrid'

const OverallScores = ({ game }) => {
  return (
    <GenericGrid>
      <Table>
        <TableBody>
          {game.players.map(player => (
            <TableRow key={player._id}>
              <TableCell>{player.name}</TableCell>
              <TableCell>{game.scores[player.sessionId].total}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </GenericGrid>
  )
}

export default OverallScores
