import React, { useContext, useState, useEffect } from 'react'
import GameContext from 'context/GameContext'
import ErrorContext from 'context/ErrorContext'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import GenericGrid from 'components/GenericGrid'
import { withStyles } from '@material-ui/core/styles'
import Die from './Die'
import Button from '@material-ui/core/Button'
import { rollDice } from 'services/games'

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit
  },
  diceHolder: {
    display: 'flex',
    justifyContent: 'space-around',
    marginBottom: theme.spacing.unit
  }
})

const Dice = ({ currentDice, isYourTurn, classes, rollsLeft }) => {
  // TODO: MAGIC NUMBER
  const defaultDiceToRoll = new Array(5).fill(null).map(_ => true)
  const [diceToRoll, setDiceToRoll] = useState(defaultDiceToRoll)
  const { setGame, setGameLoading } = useContext(GameContext)
  const { displayErrorMessage } = useContext(ErrorContext)
  useEffect(() => {
    if (!currentDice.length) {
      setDiceToRoll(defaultDiceToRoll)
    }
  }, [currentDice])
  const onRollDiceClick = async () => {
    setGameLoading(true)
    const diceToRollIds = diceToRoll.reduce(
      (acc, cur, i) => (cur ? [...acc, i] : acc),
      []
    )
    try {
      const { error } = await rollDice(diceToRollIds)
      if (error) return displayErrorMessage(error.data.message)
    } finally {
      setGameLoading(false)
    }
  }

  const onDieClick = i => {
    if (!isYourTurn) return
    setDiceToRoll(diceToRoll.map((s, idx) => (idx === i ? !s : s)))
  }

  const rollDiceMessage = (
    <Paper className={classes.paper} square>
      <Typography align="center" variant="h6">
        Roll the dice to begin turn
      </Typography>
    </Paper>
  )
  return (
    <GenericGrid>
      {currentDice && currentDice.length ? (
        <div className={classes.diceHolder}>
          {currentDice.map((die, i) => (
            <Die
              {...die}
              key={i}
              toRoll={diceToRoll[i]}
              onDieClick={() => onDieClick(i)}
            />
          ))}
        </div>
      ) : (
        rollDiceMessage
      )}
      {Boolean(isYourTurn && rollsLeft) && (
        <Button
          fullWidth
          variant="contained"
          color="secondary"
          onClick={onRollDiceClick}>
          Roll Dice ({rollsLeft}x Rolls Left)
        </Button>
      )}
    </GenericGrid>
  )
}

export default withStyles(styles)(Dice)
