import React from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import GenericGrid from 'components/GenericGrid'
import { withStyles } from '@material-ui/core/styles'
import { getIsYourTurn } from '../service'

const StyledPaper = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit
  }
}))(Paper)

const TurnIndicator = ({ sessionId, currentTurn, players }) => {
  const currentPlayerSessionId = currentTurn.playerSessionId
  const isYourTurn = getIsYourTurn({ sessionId, currentTurn })
  const currentPlayer = players.find(
    player => player.sessionId === currentPlayerSessionId
  )

  return (
    <GenericGrid>
      <StyledPaper>
        <Typography variant="h5" align="center">
          IT IS{' '}
          {isYourTurn
            ? 'YOUR'
            : `${currentPlayer.name}'${
                !currentPlayer.name.endsWith('S') ? 'S' : ''
              }`}{' '}
          TURN!
        </Typography>
      </StyledPaper>
    </GenericGrid>
  )
}

export default TurnIndicator
