import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import DICE from 'constants/dice'
import COLOR from 'constants/color'
import { withTheme } from '@material-ui/core/styles'
import { unstable_useMediaQuery as useMediaQuery } from '@material-ui/core/useMediaQuery'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = {
  toRoll: {
    opacity: 0.3
  },
  die: {
    cursor: 'pointer'
  }
}

const Die = ({ value, color, theme, toRoll, classes, onDieClick }) => {
  const diceIcon = DICE[value] && DICE[value].icon
  const diceColor = (COLOR[color] || COLOR.DEFAULT).color
  const className = useMediaQuery(theme.breakpoints.up('sm'))
    ? 'fa-6x'
    : 'fa-3x'

  const dieClassName = classNames(
    { [classes.toRoll]: toRoll },
    className,
    classes.die
  )
  return (
    <>
      {diceIcon && (
        <span className={dieClassName} onClick={onDieClick}>
          <span className="fa-layers fa-fw">
            <FontAwesomeIcon icon="square" color="white" size="sm" fixedWidth />
            <FontAwesomeIcon icon={diceIcon} color={diceColor} fixedWidth />
          </span>
        </span>
      )}
    </>
  )
}

export default withStyles(styles)(withTheme()(Die))
