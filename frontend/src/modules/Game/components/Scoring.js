import React from 'react'
import GenericGrid from 'components/GenericGrid'
import SCORING from 'constants/scoring'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import ScoringTable from './ScoringTable'

const styles = theme => ({
  scoringCell: {
    textAlign: 'center'
  },
  button: {
    marginTop: theme.spacing.unit
  }
})

const Scoring = () => {
  const scoringValues = Object.values(SCORING)
  const upperBonusScoring = scoringValues
    .filter(v => v.isUpperBonus)
    .map(v => v.value)
  const notUpperBonusScoring = scoringValues
    .filter(v => !v.isUpperBonus)
    .map(v => v.value)

  return (
    <GenericGrid>
      <Grid container spacing={8}>
        <Grid item md={6} xs={12}>
          <ScoringTable scoringFields={upperBonusScoring} isUpperBonus />
        </Grid>
        <Grid item md={6} xs={12}>
          <ScoringTable scoringFields={notUpperBonusScoring} />
        </Grid>
      </Grid>
    </GenericGrid>
  )
}

export default withStyles(styles)(Scoring)
