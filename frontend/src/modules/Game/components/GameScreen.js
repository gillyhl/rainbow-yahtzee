import React, { useContext } from 'react'
import GameContext from 'context/GameContext'
import Dice from './Dice'
import TurnIndicator from './TurnIndicator'
import Scoring from './Scoring'
import { getIsYourTurn } from '../service'
import OverallScores from './OverallScores'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import GenericGrid from 'components/GenericGrid';
import GAME_STATUS from 'constants/gameStatus'

const styles = theme => ({
  button: {
    marginTop: theme.spacing.unit * 8
  }
})

const GameScreen = ({ classes, history }) => {
  const { game } = useContext(GameContext)
  const isYourTurn = game && getIsYourTurn(game)
  if (game && game.status === GAME_STATUS.FINISHED.value) history.push(GAME_STATUS.FINISHED.uri)
  return (
    <main>
      {game && (
        <>
          <TurnIndicator {...game} />
          <Dice
            isYourTurn={isYourTurn}
            currentDice={game.currentTurn.dice}
            rollsLeft={game.currentTurn.rollsLeft}
          />
          <OverallScores game={game} />
          <Scoring />
        </>
      )}
      {!game && <GenericGrid><Button className={classes.button} variant='contained' color='secondary' fullWidth href='/'>
        Return Home
      </Button></GenericGrid>}
    </main>
  )
}

export default withStyles(styles)(GameScreen)
