export const getIsYourTurn = ({ currentTurn, sessionId }) =>
  currentTurn.playerSessionId === sessionId
