import React, { useContext } from 'react'
import GenericGrid from 'components/GenericGrid'
import GameContext from 'context/GameContext'
import ErrorContext from 'context/ErrorContext'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import _ from 'lodash'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import Button from '@material-ui/core/Button'
import { deleteGame } from 'services/games'
import { subscribeToDeletedGame } from 'socket'

const styles = theme => ({
  currentSession: {
    background: theme.palette.secondary.main
  },
  button: {
    marginTop: theme.spacing.unit
  }
})

const ResultsScreen = ({ classes, history }) => {
  const { game, setGameLoading, setGame } = useContext(GameContext)
  const { displayErrorMessage } = useContext(ErrorContext)
  subscribeToDeletedGame(() => {
    setGame(null)
    history.push('/')
  })
  const results =
    game &&
    _.chain(game.scores)
      .reduce(
        (acc, cur, k) => [
          ...acc,
          {
            player: game.players.find(player => player.sessionId === k),
            score: cur.total,
            isCurrentSession: k === game.sessionId
          }
        ],
        []
      )
      .orderBy(['score'], ['desc'])
      .map((result, i, array) => ({
        ...result,
        rank: array.findIndex(v => v.score === result.score) + 1
      }))
      .value()

  const onCompleteGameClick = async () => {
    setGameLoading(true)
    const { error } = await deleteGame()
    if (error) return displayErrorMessage(error.data.message)
    setGame(null)
    setGameLoading(false)
    history.push('/')
  }
  return (
    <GenericGrid>
      {results && (
        <>
          <Typography variant="h3" align="center">
            Results
          </Typography>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell>Player</TableCell>
                <TableCell>Score</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {results.map(result => (
                <TableRow
                  key={result.player._id}
                  className={classNames({
                    [classes.currentSession]: result.isCurrentSession
                  })}>
                  <TableCell>{result.rank}</TableCell>
                  <TableCell>{result.player.name}</TableCell>
                  <TableCell>{result.score}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {game.gameCreator.sessionId === game.sessionId && (
            <Button
              fullWidth
              variant="contained"
              color="primary"
              onClick={onCompleteGameClick}
              className={classes.button}>
              Complete Game
            </Button>
          )}
        </>
      )}
    </GenericGrid>
  )
}

export default withStyles(styles)(ResultsScreen)
