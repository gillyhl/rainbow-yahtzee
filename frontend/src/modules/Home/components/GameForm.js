import React, { useEffect } from 'react'
import useForm from 'hooks/useForm'
import TextField from '@material-ui/core/TextField'

const GameForm = ({ onFormDataChange }) => {
  const formMeta = {
    name: {
      value: '',
      onChange: e => e.target.value.toUpperCase()
    },
    code: {
      value: '',
      onChange: e => e.target.value.replace(/\D/g, '')
    }
  }

  const validators = {
    name: [
      {
        isValid: v => Boolean(v.name),
        message: 'Name is required'
      },
      {
        isValid: v => v.name && v.name.length > 2,
        message: 'Name should be at least 3 characters long'
      }
    ],
    code: [
      {
        isValid: v => v.code && v.code.replace(' ', '').match(/^\d{6}$/),
        message: 'Code must be 6 digits long'
      }
    ]
  }
  const [formData, errors, , values, { isValidField }] = useForm(
    formMeta,
    validators
  )

  useEffect(() => {
    onFormDataChange({
      values,
      valid: {
        name: isValidField('name'),
        code: isValidField('code')
      },
      errors
    })
  }, [formData])
  return (
    <section>
      <TextField {...formData.name} fullWidth label="Player Name" />
      <TextField {...formData.code} fullWidth label="Game Code" />
    </section>
  )
}

export default GameForm
