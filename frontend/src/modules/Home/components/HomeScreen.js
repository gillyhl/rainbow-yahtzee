import React, { useContext } from 'react'
import GameContext from 'context/GameContext'
import ErrorContext from 'context/ErrorContext'
import HomeCard from './HomeCard'
import GenericGrid from 'components/GenericGrid'
import { withStyles } from '@material-ui/core/styles'
import { getGame } from 'services/games'
import useGenericLoadData from 'hooks/useGenericLoadData'

const styles = theme => ({
  card: {
    margin: `${theme.spacing.unit * 8}px auto`,
    maxWidth: '400px'
  }
})

const HomeScreen = ({ classes }) => {
  const { setGame, setGameLoading } = useContext(GameContext)
  const { displayErrorMessage } = useContext(ErrorContext)
  useGenericLoadData(
    () => getGame(),
    [],
    setGame,
    displayErrorMessage,
    setGameLoading
  )

  return (
    <main>
      <GenericGrid>
        <HomeCard className={classes.card} />
      </GenericGrid>
    </main>
  )
}

export default withStyles(styles)(HomeScreen)
