import React, { useState, useContext } from 'react'
import ErrorContext from 'context/ErrorContext'
import GameContext from 'context/GameContext'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import GameForm from './GameForm'
import CardContent from '@material-ui/core/CardContent'
import { createGame, joinGame } from 'services/games'
import { withRouter } from 'react-router-dom'
import ButtonWithLoadingProgress from 'components/ButtonWithLoadingProgress'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import GAME_STATUS from 'constants/gameStatus'

const getFirstError = errors =>
  _.first(
    _.chain(errors)
      .values()
      .flatten()
      .value()
  )

const HomeCard = ({ className, history }) => {
  const [formData, setFormData] = useState(null)
  const [createGameLoading, setCreateGameLoading] = useState(false)
  const [joinGameLoading, setJoinGameLoading] = useState(false)
  const { displayErrorMessage } = useContext(ErrorContext)
  const { game, setGame } = useContext(GameContext)
  const onFormDataChange = data => {
    setFormData(data)
  }

  const onCreateGameClick = async () => {
    const { values, errors, valid } = formData
    if (!valid.name) return displayErrorMessage(getFirstError(errors))

    setCreateGameLoading(true)
    const { result, error } = await createGame(values.name)
    setCreateGameLoading(false)
    if (error) return displayErrorMessage(error.data.message)
    else {
      setGame(result.data)
      history.push('/lobby')
    }
  }

  const onJoinGameClick = async () => {
    const { values, errors, valid } = formData
    if (!valid.name || !valid.code)
      return displayErrorMessage(getFirstError(errors))

    setJoinGameLoading(true)
    const { error } = await joinGame(values.name, values.code)
    setJoinGameLoading(false)
    if (error) return displayErrorMessage(error.data.message)
    else history.push('/lobby')
  }

  const noGameActions = (
    <CardActions>
      <ButtonWithLoadingProgress
        variant="contained"
        color="primary"
        loading={createGameLoading}
        onClick={onCreateGameClick}
        fullWidth>
        Create Game
      </ButtonWithLoadingProgress>
      <ButtonWithLoadingProgress
        variant="contained"
        color="primary"
        loading={joinGameLoading}
        onClick={onJoinGameClick}
        fullWidth>
        Join Game
      </ButtonWithLoadingProgress>
    </CardActions>
  )

  return (
    <Card className={className}>
      <CardHeader title="Rainbow Yahtzee" />
      <CardContent>
        <GameForm onFormDataChange={onFormDataChange} />
      </CardContent>
      {game ? (
        <CardActions>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            component={Link}
            to={GAME_STATUS[game.status] && GAME_STATUS[game.status].uri}>
            Rejoin Game
          </Button>
        </CardActions>
      ) : (
        noGameActions
      )}
    </Card>
  )
}

export default withRouter(HomeCard)
