import React, { useContext, useEffect } from 'react'
import GameContext from 'context/GameContext'
import ErrorContext from 'context/ErrorContext'
import GenericGrid from 'components/GenericGrid'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import GameCode from './GameCode'
import Players from './Players'
import { deleteGame, leaveGame, startGame } from 'services/games'
import GAME_STATUS from 'constants/gameStatus'

const styles = theme => ({
  grid: {
    marginTop: theme.spacing.unit
  },
  loading: {
    margin: '0 auto'
  },
  button: {
    marginBottom: theme.spacing.unit
  }
})

const GameLobbyScreen = ({ classes, history }) => {
  const { game, setGameLoading } = useContext(GameContext)
  const { displayErrorMessage } = useContext(ErrorContext)

  // TODO: Make these calls DRY
  const onDeleteGameClick = async () => {
    setGameLoading(true)
    try {
      const { error } = await deleteGame()
      setGameLoading(false)
      if (error) displayErrorMessage(error.data.message)
      // TODO: Use notification snack bar to show successful deletion
      // TODO: Notify of successful deletion to other clients in game.
      else history.push('/')
    } catch (e) {
      setGameLoading(false)
    }
  }

  const onLeaveGameClick = async () => {
    setGameLoading(true)
    try {
      const { error } = await leaveGame()
      setGameLoading(false)
      if (error) displayErrorMessage(error.data.message)
      else history.push('/')
    } catch (e) {
      setGameLoading(false)
    }
  }

  const onStartGameClick = async () => {
    setGameLoading(true)
    try {
      const { error } = await startGame()
      setGameLoading(false)
      if (error) displayErrorMessage(error.data.message)
      else history.push('/game')
    } catch (e) {
      setGameLoading(false)
    }
  }

  useEffect(() => {
    if (game) {
      const { status } = game
      if (status === GAME_STATUS.PLAYING.value)
        history.push(GAME_STATUS.PLAYING.uri)
    }
  }, [game])

  return (
    <main>
      <GenericGrid>
        {game && (
          <Grid container spacing={16} className={classes.grid}>
            <Grid item md={6} xs={12}>
              <GameCode code={game.code} />
            </Grid>
            <Grid item md={6} xs={12}>
              <Players players={game.players} />
            </Grid>
            {game.sessionId !== game.gameCreator.sessionId && (
              <Button
                fullWidth
                variant="contained"
                color="secondary"
                onClick={onLeaveGameClick}
                className={classes.button}>
                Leave Game
              </Button>
            )}
            {game.sessionId === game.gameCreator.sessionId && (
              <Button
                fullWidth
                variant="contained"
                color="secondary"
                onClick={onDeleteGameClick}
                className={classes.button}>
                Delete Game
              </Button>
            )}
            {game.sessionId === game.gameCreator.sessionId &&
              game.players.length > 1 && (
                <Button
                  fullWidth
                  variant="contained"
                  color="secondary"
                  onClick={onStartGameClick}
                  className={classes.button}>
                  Start Game
                </Button>
              )}
          </Grid>
        )}
      </GenericGrid>
    </main>
  )
}

export default withStyles(styles)(GameLobbyScreen)
