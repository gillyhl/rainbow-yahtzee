import React from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'

const StyledPaper = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit
  }
}))(Paper)

const GameCode = ({ code }) => {
  return (
    <>
      <Typography variant="h4" align="center">
        Game Code
      </Typography>
      <StyledPaper>
        <Typography variant="h2" align="center">
          {code.split('').join(' ')}
        </Typography>
      </StyledPaper>
    </>
  )
}

export default GameCode
