import React from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'

const StyledPaper = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit
  }
}))(Paper)

const Players = ({ players }) => {
  return (
    <section>
      <Typography variant="h4" align="center">
        Players
      </Typography>
      {players &&
        players.map(player => (
          <StyledPaper key={player._id}>
            <Typography variant="h2" align="center">
              {player.name}
            </Typography>
          </StyledPaper>
        ))}
    </section>
  )
}

export default Players
