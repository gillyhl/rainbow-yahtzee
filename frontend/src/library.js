import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faDiceOne,
  faDiceTwo,
  faDiceThree,
  faDiceFour,
  faDiceFive,
  faDiceSix,
  faSquare
} from '@fortawesome/free-solid-svg-icons'

library.add(faDiceOne)
library.add(faDiceTwo)
library.add(faDiceThree)
library.add(faDiceFour)
library.add(faDiceFive)
library.add(faDiceSix)
library.add(faSquare)
