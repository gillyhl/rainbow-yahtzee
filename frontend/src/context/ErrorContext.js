import React from 'react'

const defaultValue = {
  displayErrorMessage: message => {}
}

export default React.createContext(defaultValue)
