import React from 'react'

const defaultValue = {
  game: null,
  setGame: () => {},
  gameLoading: false,
  setGameLoading: () => {}
}

export default React.createContext(defaultValue)
