import red from '@material-ui/core/colors/red'
import green from '@material-ui/core/colors/green'
import blue from '@material-ui/core/colors/blue'
import grey from '@material-ui/core/colors/grey'

export default {
  RED: { color: red[600], value: 'RED' },
  GREEN: { color: green[800], value: 'GREEN' },
  BLUE: { color: blue[600], value: 'BLUE' },
  DEFAULT: { color: grey[700], value: 'DEFAULT' }
}
