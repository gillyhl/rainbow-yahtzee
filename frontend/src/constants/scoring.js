import DICE from 'constants/dice'
import COLORS from 'constants/color'
import _ from 'lodash'

const upperSectionScoring = die => dice =>
  dice.filter(({ value }) => value === die.value).length * die.numValue

const sumDice = dice => _.sum(dice.map(die => DICE[die.value].numValue))

const groupDice = groupSize => dice => {
  const success = _.chain(dice)
    .groupBy(({ value }) => value)
    .some(v => v.length >= groupSize)
    .value()
  return success ? sumDice(dice) : 0
}

const scoreGenericFullHouse = (property, score) => dice => {
  const groups = _.chain(dice)
    .groupBy(die => die[property])
    .reduce((acc, cur) => {
      const isDouble = cur.length === 2
      const isTriple = cur.length === 3
      return {
        ...acc,
        ...(isDouble && { hasDouble: true }),
        ...(isTriple && { hasTriple: true })
      }
    }, {})
    .value()

  return groups.hasDouble && groups.hasTriple ? score : 0
}

const fullHouse = scoreGenericFullHouse('value', 25)

const scoreColorFullHouse = scoreGenericFullHouse('color', 15)

const yahtzee = dice =>
  dice.every(({ value }) => value === dice[0].value) && dice.length ? 50 : 0

const scoreColor = inputColor => dice =>
  dice.every(({ color }) => color === inputColor.value) && dice.length ? 35 : 0

const getStraightParsedDice = dice =>
  _.chain(dice)
    .map(({ value }) => DICE[value].numValue)
    .orderBy()
    .value()
    .join('')

const smallStraight = dice => {
  return /12+3+4|23+4+5|34+5+6/.test(getStraightParsedDice(dice)) ? 30 : 0
}

const largeStraight = dice => {
  return /12+3+4+5|23+4+5+6/.test(getStraightParsedDice(dice)) ? 40 : 0
}

export default {
  ONES: {
    value: 'ONES',
    score: upperSectionScoring(DICE.ONE),
    isUpperBonus: true
  },
  TWOS: {
    value: 'TWOS',
    score: upperSectionScoring(DICE.TWO),
    isUpperBonus: true
  },
  THREES: {
    value: 'THREES',
    score: upperSectionScoring(DICE.THREE),
    isUpperBonus: true
  },
  FOURS: {
    value: 'FOURS',
    score: upperSectionScoring(DICE.FOUR),
    isUpperBonus: true
  },
  FIVES: {
    value: 'FIVES',
    score: upperSectionScoring(DICE.FIVE),
    isUpperBonus: true
  },
  SIXES: {
    value: 'SIXES',
    score: upperSectionScoring(DICE.SIX),
    isUpperBonus: true
  },
  THREE_TIMES: {
    value: 'THREE_TIMES',
    score: groupDice(3)
  },
  UPPER_BONUS: {
    value: 'UPPER_BONUS',
    isUpperBonus: true
  },
  FOUR_TIMES: { value: 'FOUR_TIMES', score: groupDice(4) },
  FULL_HOUSE: { value: 'FULL_HOUSE', score: fullHouse },
  SMALL_STRAIGHT: { value: 'SMALL_STRAIGHT', score: smallStraight },
  LARGE_STRAIGHT: { value: 'LARGE_STRAIGHT', score: largeStraight },
  CHANCE: { value: 'CHANCE', score: sumDice },
  GREENS: { value: 'GREENS', score: scoreColor(COLORS.GREEN) },
  REDS: { value: 'REDS', score: scoreColor(COLORS.RED) },
  BLUES: { value: 'BLUES', score: scoreColor(COLORS.BLUE) },
  COLOR_FULL_HOUSE: { value: 'COLOR_FULL_HOUSE', score: scoreColorFullHouse },
  YAHTZEE: { value: 'YAHTZEE', score: yahtzee },
  YAHTZEE_BONUS: { value: 'YAHTZEE_BONUS' }
}
