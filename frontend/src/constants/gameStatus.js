export default {
  WAITING_FOR_PLAYERS: { value: 'WAITING_FOR_PLAYERS', uri: '/lobby' },
  PLAYING: { value: 'PLAYING', uri: '/game' },
  FINISHED: { value: 'FINISHED', uri: '/results' }
}
