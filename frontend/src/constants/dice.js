export default {
  ONE: {
    icon: 'dice-one',
    value: 'ONE',
    numValue: 1
  },
  TWO: {
    icon: 'dice-two',
    value: 'TWO',
    numValue: 2
  },
  THREE: {
    icon: 'dice-three',
    value: 'THREE',
    numValue: 3
  },
  FOUR: {
    icon: 'dice-four',
    value: 'FOUR',
    numValue: 4
  },
  FIVE: {
    icon: 'dice-five',
    value: 'FIVE',
    numValue: 5
  },
  SIX: {
    icon: 'dice-six',
    value: 'SIX',
    numValue: 6
  }
}
