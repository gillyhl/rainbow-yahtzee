import React, { useState } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import { MuiThemeProvider } from '@material-ui/core/styles'
import HomeScreen from 'modules/Home/components/HomeScreen'
import GameLobbyScreen from 'modules/GameLobby/components/GameLobbyScreen'
import GameScreen from 'modules/Game/components/GameScreen'
import ResultsScreen from 'modules/Results/components/ResultsScreen'
import GameContext from 'context/GameContext'
import ErrorContext from 'context/ErrorContext'
import ErrorSnackBar from 'components/ErrorSnackBar'
import { Route } from 'react-router-dom'
import theme from 'theme'
import { subscribeToRefresh } from 'socket'
import { getGame } from 'services/games'
import useGenericLoadData from 'hooks/useGenericLoadData'
import Loading from 'components/Loading'

const App = () => {
  const [game, setGame] = useState(null)
  const [showError, setShowError] = useState(false)
  const [gameLoading, setGameLoading] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)
  const displayErrorMessage = message => {
    setShowError(true)
    setErrorMessage(message)
  }
  useGenericLoadData(
    () => getGame(),
    [],
    setGame,
    displayErrorMessage,
    setGameLoading
  )
  subscribeToRefresh(data => setGame(data))
  const handleErrorMessageClose = () => {
    setShowError(false)
  }

  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      {gameLoading && <Loading />}
      <ErrorContext.Provider value={{ displayErrorMessage }}>
        <GameContext.Provider
          value={{ game, setGame, gameLoading, setGameLoading }}>
          <Route exact path="/" component={HomeScreen} />
          <Route exact path="/lobby" component={GameLobbyScreen} />
          <Route exact path="/game" component={GameScreen} />
          <Route exact path="/results" component={ResultsScreen} />
        </GameContext.Provider>
        <ErrorSnackBar
          open={showError}
          message={errorMessage}
          handleClose={handleErrorMessageClose}
        />
      </ErrorContext.Provider>
    </MuiThemeProvider>
  )
}

export default App
