import { createMuiTheme } from '@material-ui/core/styles'
const theme = createMuiTheme({
  palette: {
    type: 'dark'
  },
  typography: {
    useNextVariants: true
  }
})

theme.overrides = {
  MuiTableRow: {
    root: {
      height: 30
    }
  },
  MuiTableCell: {
    root: {
      [theme.breakpoints.down('md')]: {
        padding: 5
      }
    }
  }
}

export default theme
