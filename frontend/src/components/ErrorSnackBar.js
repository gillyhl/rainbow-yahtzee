import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'

const ErrorSnackbar = ({ message, open, handleClose }) => {
  return (
    <Snackbar
      open={open}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left'
      }}
      autoHideDuration={6000}
      onClose={handleClose}
      message={<span>{message}</span>}
    />
  )
}

export default ErrorSnackbar
