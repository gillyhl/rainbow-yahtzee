import React from 'react'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  grid: {
    marginTop: theme.spacing.unit
  }
})

const GenericGrid = ({ children, classes }) => {
  return (
    <Grid container spacing={0} className={classes.grid}>
      <Grid item md={2} xs={1} />
      <Grid item md={8} xs={10}>
        {children}
      </Grid>
      <Grid item md={2} xs={1} />
    </Grid>
  )
}

export default withStyles(styles)(GenericGrid)
