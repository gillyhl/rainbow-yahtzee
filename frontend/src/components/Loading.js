import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { withTheme } from '@material-ui/core/styles'
import DICE from 'constants/dice'
import pickBy from 'lodash/pickBy'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  loader: {
    display: 'block',
    margin: `${theme.spacing.unit * 8}px auto`
  },
  container: {
    height: '100vh',
    opacity: 0.9,
    zIndex: 1000,
    background: theme.palette.background.default,
    position: 'absolute',
    top: 0,
    width: '100%'
  }
})

const Loading = ({ theme, className, classes }) => {
  const getRandomDiceIcon = currentDice => {
    const filteredDice = pickBy(DICE, v => !v || v.icon !== currentDice)
    const keyIdx = Math.floor(Math.random() * Object.keys(filteredDice).length)
    return filteredDice[Object.keys(filteredDice)[keyIdx]].icon
  }
  const [dice, setDice] = useState(getRandomDiceIcon(null))

  const setNewDice = () => {
    setDice(dice => getRandomDiceIcon(dice))
  }

  useEffect(() => {
    const id = setInterval(setNewDice, 200)
    return () => clearInterval(id)
  }, [])

  return (
    <div className={classes.container}>
      <FontAwesomeIcon
        className={classes.loader}
        icon={dice}
        color={theme.palette.secondary.light}
        size="6x"
      />
    </div>
  )
}

export default withStyles(styles)(withTheme()(Loading))
