import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import green from '@material-ui/core/colors/green'
import classNames from 'classnames'

const styles = {
  wrapper: {
    position: 'relative',
    width: '100%'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  }
}

const ButtonWithLoadingProgress = ({
  className,
  classes,
  loading,
  children,
  ...rest
}) => {
  return (
    <div className={classNames(classes.wrapper, className)}>
      <Button disabled={loading} {...rest}>
        {children}
      </Button>
      {loading && (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </div>
  )
}

export default withStyles(styles)(ButtonWithLoadingProgress)
