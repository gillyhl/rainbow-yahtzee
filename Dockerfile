FROM node:9.11
RUN mkdir /usr/src/app/
RUN mkdir /usr/src/app/frontend
RUN mkdir /usr/src/app/backend
WORKDIR /usr/src/app/frontend
COPY ./frontend/package.json .
RUN npm install --silent
COPY ./frontend .
RUN npm run build
WORKDIR /usr/src/app/backend
COPY backend/package.json .
RUN npm install --silent
COPY ./backend .
CMD ["npm", "start"]


