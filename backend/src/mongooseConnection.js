const mongoose = require('mongoose')
const uri = process.env.MONGODB_URI || 'mongodb://localhost:27017/yahtzee'
module.exports = () => mongoose.createConnection(uri, { useNewUrlParser: true })
