const HttpStatus = require('http-status-codes')
class Unauthorized extends Error {
  constructor(...args) {
    super(...args)
    Error.captureStackTrace(this, Unauthorized)
    this.statusCode = HttpStatus.UNAUTHORIZED
  }
}

class BadRequest extends Error {
  constructor(...args) {
    super(...args)
    Error.captureStackTrace(this, BadRequest)
    this.statusCode = HttpStatus.BAD_REQUEST
  }
}

module.exports = {
  Unauthorized,
  BadRequest
}
