const express = require('express')
const app = express()
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const glob = require('glob')
const path = require('path')
const bodyParser = require('body-parser')

const port = process.env.PORT || 5000
const sessionSecret = process.env.SESSION_SECRET || 'dev-secret'
const mongooseConnection = require('./mongooseConnection')()

const sessionStore = new MongoStore({ mongooseConnection })
app.use(bodyParser.json())
const sessionMiddleware = session({
  secret: sessionSecret,
  store: sessionStore,
  unset: 'destroy',
  resave: false,
  saveUninitialized: true
})
app.use(sessionMiddleware)

const router = express.Router()
app.use('/api/v1', router)

const http = require('http').Server(app)
const io = require('socket.io')(http)

io.use(async (socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next)
})
io.on('connect', socket => {
  // socket.on('session', () => {})
})

const matches = glob.sync(path.join(__dirname, 'modules/**/controller.js'))
matches.forEach(match => {
  require(match)(router, io)
})

app.use(express.static(path.join(__dirname, '../../frontend/build')))
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../../frontend/build/index.html'))
})

http.listen(port, () => {
  console.log(`Yahtzee App listening on port ${port}`)
})
