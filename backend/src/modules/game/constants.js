const _ = require('lodash')
const GAME_STATUS = {
  WAITING_FOR_PLAYERS: 'WAITING_FOR_PLAYERS',
  PLAYING: 'PLAYING',
  FINISHED: 'FINISHED'
}

const COLORS = {
  RED: { value: 'RED' },
  GREEN: { value: 'GREEN' },
  BLUE: { value: 'BLUE' }
}

const SCORING_INIT = {
  ONES: { value: 'ONES', default: null },
  TWOS: { value: 'TWOS', default: null },
  THREES: { value: 'THREES', default: null },
  FOURS: { value: 'FOURS', default: null },
  FIVES: { value: 'FIVES', default: null },
  SIXES: { value: 'SIXES', default: null },
  UPPER_BONUS: {
    value: 'UPPER_BONUS',
    default: 0
  },
  THREE_TIMES: { value: 'THREE_TIMES', default: null },
  FOUR_TIMES: { value: 'FOUR_TIMES', default: null },
  FULL_HOUSE: { value: 'FULL_HOUSE', default: null },
  SMALL_STRAIGHT: { value: 'SMALL_STRAIGHT', default: null },
  LARGE_STRAIGHT: { value: 'LARGE_STRAIGHT', default: null },
  YAHTZEE: { value: 'YAHTZEE', default: null },
  CHANCE: { value: 'CHANCE', default: null },
  YAHTZEE_BONUS: { value: 'YAHTZEE_BONUS', default: 0 },
  GREENS: { value: 'GREENS', default: null },
  REDS: { value: 'REDS', default: null },
  BLUES: { value: 'BLUES', default: null },
  COLOR_FULL_HOUSE: { value: 'COLOR_FULL_HOUSE', default: null }
}

const DICE = {
  ONE: {
    icon: 'dice-one',
    value: 'ONE',
    numValue: 1
  },
  TWO: {
    icon: 'dice-two',
    value: 'TWO',
    numValue: 2
  },
  THREE: {
    icon: 'dice-three',
    value: 'THREE',
    numValue: 3
  },
  FOUR: {
    icon: 'dice-four',
    value: 'FOUR',
    numValue: 4
  },
  FIVE: {
    icon: 'dice-five',
    value: 'FIVE',
    numValue: 5
  },
  SIX: {
    icon: 'dice-six',
    value: 'SIX',
    numValue: 6
  }
}

const UPPER_BONUS_FIELDS = ['ONES', 'TWOS', 'THREES', 'FOURS', 'FIVES', 'SIXES']

const upperSectionScoring = die => dice =>
  dice.filter(({ value }) => value === die.value).length * die.numValue

const sumDice = dice => _.sum(dice.map(die => DICE[die.value].numValue))

const groupDice = groupSize => dice => {
  const success = _.chain(dice)
    .groupBy(({ value }) => value)
    .some(v => v.length >= groupSize)
    .value()
  return success ? sumDice(dice) : 0
}

const scoreGenericFullHouse = (property, score) => dice => {
  const groups = _.chain(dice)
    .groupBy(die => die[property])
    .reduce((acc, cur) => {
      const isDouble = cur.length === 2
      const isTriple = cur.length === 3
      return {
        ...acc,
        ...(isDouble && { hasDouble: true }),
        ...(isTriple && { hasTriple: true })
      }
    }, {})
    .value()

  return groups.hasDouble && groups.hasTriple ? score : 0
}

const fullHouse = scoreGenericFullHouse('value', 25)

const scoreColorFullHouse = scoreGenericFullHouse('color', 15)

const yahtzee = dice =>
  dice.every(({ value }) => value === dice[0].value) && dice.length ? 50 : 0

const scoreColor = inputColor => dice =>
  dice.every(({ color }) => color === inputColor.value) && dice.length ? 35 : 0

const getStraightParsedDice = dice =>
  _.chain(dice)
    .map(({ value }) => DICE[value].numValue)
    .orderBy()
    .value()
    .join('')

const smallStraight = dice => {
  return /12+3+4|23+4+5|34+5+6/.test(getStraightParsedDice(dice)) ? 30 : 0
}

const largeStraight = dice => {
  return /12+3+4+5|23+4+5+6/.test(getStraightParsedDice(dice)) ? 40 : 0
}

const SCORING = {
  ONES: {
    value: 'ONES',
    score: upperSectionScoring(DICE.ONE)
  },
  TWOS: {
    value: 'TWOS',
    score: upperSectionScoring(DICE.TWO)
  },
  THREES: {
    value: 'THREES',
    score: upperSectionScoring(DICE.THREE)
  },
  FOURS: {
    value: 'FOURS',
    score: upperSectionScoring(DICE.FOUR)
  },
  FIVES: {
    value: 'FIVES',
    score: upperSectionScoring(DICE.FIVE)
  },
  SIXES: {
    value: 'SIXES',
    score: upperSectionScoring(DICE.SIX)
  },
  UPPER_BONUS: {
    value: 'UPPER_BONUS'
  },
  THREE_TIMES: {
    value: 'THREE_TIMES',
    score: groupDice(3)
  },
  FOUR_TIMES: { value: 'FOUR_TIMES', score: groupDice(4) },
  FULL_HOUSE: { value: 'FULL_HOUSE', score: fullHouse, jokerScore: 25 },
  SMALL_STRAIGHT: {
    value: 'SMALL_STRAIGHT',
    score: smallStraight,
    jokerScore: 30
  },
  LARGE_STRAIGHT: {
    value: 'LARGE_STRAIGHT',
    score: largeStraight,
    jokerScore: 40
  },
  CHANCE: { value: 'CHANCE', score: sumDice },
  GREENS: { value: 'GREENS', score: scoreColor(COLORS.GREEN) },
  REDS: { value: 'REDS', score: scoreColor(COLORS.RED) },
  BLUES: { value: 'BLUES', score: scoreColor(COLORS.BLUE) },
  COLOR_FULL_HOUSE: { value: 'COLOR_FULL_HOUSE', score: scoreColorFullHouse },
  YAHTZEE: { value: 'YAHTZEE', score: yahtzee },
  YAHTZEE_BONUS: { value: 'YAHTZEE_BONUS' }
}

const NUMBER_OF_DICE = 5

module.exports = {
  GAME_STATUS,
  COLORS,
  SCORING,
  SCORING_INIT,
  DICE,
  NUMBER_OF_DICE,
  UPPER_BONUS_FIELDS
}
