const { model } = require('./model')
const mongooseConnection = require('../../mongooseConnection')
const { Unauthorized, BadRequest } = require('../../errors')
const {
  GAME_STATUS,
  SCORING,
  SCORING_INIT,
  NUMBER_OF_DICE,
  UPPER_BONUS_FIELDS
} = require('./constants')
const mapValues = require('lodash/mapValues')
const sum = require('lodash/sum')
const every = require('lodash/every')
const { randomDiceRoll } = require('../../services/dice')
const { NODE_ENV } = process.env

const CODE_LENGTH = 6

const getSessionGame = async session => {
  if (!session || !session.id) return null
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    const data = await Game.findOne({
      'players.sessionId': session.id
    }).lean()

    return (
      data && {
        ...data,
        sessionId: session.id
      }
    )
  } finally {
    connection.close()
  }
}

const createGame = async (session, name) => {
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    const player = {
      sessionId: session.id,
      name
    }
    const code = await _getGameCode(Game)
    return await Game.create({
      code,
      players: [player],
      gameCreator: player,
      currentTurn: {
        playerSessionId: session.id
      }
    })
  } finally {
    connection.close()
  }
}

const addPlayerToGame = async (code, session, name, io) => {
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    // TODO: Check game code exists
    const data = await Game.findOneAndUpdate(
      {
        code
      },
      {
        $push: {
          players: {
            sessionId: session.id,
            name
          }
        }
      },
      { new: true }
    )
    _refreshGames(data, io)
    return data
  } finally {
    connection.close()
  }
}

const deleteGame = async (session, io) => {
  const game = await _getGameAsCreator(session)
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    const result = await Game.deleteOne({ 'gameCreator.sessionId': session.id })
    _notifyDeletedGame(game, io)
    return result
  } finally {
    connection.close()
  }
}

const removePlayerFromGame = async (session, io) => {
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    const game = await Game.findOneAndUpdate(
      { 'players.sessionId': session.id },
      {
        $pull: {
          players: {
            sessionId: session.id
          }
        }
      },
      { new: true }
    )
    _refreshGames(game, io)
  } finally {
    connection.close()
  }
}

const startGame = async (session, io) => {
  const game = await _getGameAsCreator(session)
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    const updatedGame = await Game.findOneAndUpdate(
      { 'players.sessionId': session.id },
      {
        status: GAME_STATUS.PLAYING,
        scores: game.players.reduce(
          (acc, cur) => ({
            ...acc,
            [cur.sessionId]: {
              total: 0,
              meta: mapValues(SCORING_INIT, v => v.default)
            }
          }),
          {}
        )
      },
      { new: true }
    )
    _refreshGames(updatedGame, io)
  } finally {
    connection.close()
  }
}

// TODO: Check player has rolls left
const rollDice = async (session, dice, io) => {
  const { currentTurn } = await _getGameAsCurrentPlayer(session)
  const connection = mongooseConnection()
  const Game = model(connection)
  const newDice = currentTurn.dice.length
    ? currentTurn.dice.map((d, i) => {
        const isToBeReplaced = dice.some(di => di === i)
        return isToBeReplaced ? randomDiceRoll() : d
      })
    : new Array(NUMBER_OF_DICE).fill(null).map(_ => randomDiceRoll())
  try {
    const updatedGame = await Game.findOneAndUpdate(
      { 'players.sessionId': session.id },
      {
        'currentTurn.dice': newDice,
        $inc: { 'currentTurn.rollsLeft': -1 }
      },
      { new: true }
    )
    _refreshGames(updatedGame, io)
    return updatedGame
  } finally {
    connection.close()
  }
}

const scoreDice = async (session, scoreField, io) => {
  const game = await _getGameAsCurrentPlayer(session)
  const newScores = _getNewScore(session.id, game, scoreField)
  const connection = mongooseConnection()
  const Game = model(connection)
  try {
    const updatedGame = await Game.findOneAndUpdate(
      { 'players.sessionId': session.id },
      {
        [`scores.${session.id}`]: newScores,
        currentTurn: {
          // TODO: MAGIC NUMBER
          rollsLeft: 3,
          dice: [],
          playerSessionId:
            game.players[
              (game.players.findIndex(
                v => v.sessionId === game.currentTurn.playerSessionId
              ) +
                1) %
                game.players.length
            ].sessionId
        },
        status: _isGameComplete({
          ...game.scores,
          [session.id]: newScores
        })
          ? GAME_STATUS.FINISHED
          : GAME_STATUS.PLAYING
      },
      { new: true }
    )
    _refreshGames(updatedGame, io)
    return updatedGame
  } finally {
    connection.close()
  }
}

const _getNewScore = (sessionId, game, scoreField) => {
  const {
    currentTurn: { dice },
    scores
  } = game
  const currentScoring = scores[sessionId]
  if (currentScoring.meta[scoreField])
    throw new BadRequest('User has already scored for this section')
  const score = _getCurrentDiceScore(dice, scoreField, currentScoring)
  const oldUpperBonus = currentScoring.meta.UPPER_BONUS
  const newUpperBonus = _upperBonusCheck({
    ...currentScoring.meta,
    [scoreField]: score
  })
  return {
    ...currentScoring,
    total: currentScoring.total + score + (oldUpperBonus ? 0 : newUpperBonus),
    meta: {
      ...currentScoring.meta,
      [scoreField]: score,
      UPPER_BONUS: newUpperBonus
    }
  }
}

const _getCurrentDiceScore = (dice, scoreField, currentScoring) => {
  const scoreHasYahtzee = _scoreHasYahtzee(currentScoring)
  const diceHasYahtzee = _isYahtzee(dice)
  const scoring = SCORING[scoreField]
  return (
    (scoreHasYahtzee && diceHasYahtzee && scoring.jokerScore) ||
    scoring.score(dice)
  )
}

/**
 * Get game object for request session, if the sesion isn't the game
 * creator, throw unauthorised.
 * @param {Session} session request session
 * @throws {Unauthorized} If game belonging to session isn't session's created game
 */
const _getGameAsCreator = async session => {
  const game = await getSessionGame(session)
  if (game.sessionId !== game.gameCreator.sessionId)
    throw new Unauthorized('You are not the game creator')
  return game
}

const _getGameAsCurrentPlayer = async session => {
  const game = await getSessionGame(session)
  if (game.sessionId !== game.currentTurn.playerSessionId)
    throw new Unauthorized('You are not the current player')
  return game
}

const _getGameCode = async Game => {
  while (true) {
    const code = _generateRandomCode()
    const game = await Game.findOne({ code })
    if (!game) return code
  }
}

const _generateRandomCode = () =>
  new Array(CODE_LENGTH)
    .fill(null)
    .map(_ => Math.floor(Math.random() * 10))
    .join('')

const _refreshGames = (game, io) =>
  Object.values(io.sockets.sockets)
    .filter(socket =>
      game.players.some(
        player => player.sessionId === socket.request.session.id
      )
    )
    .forEach(socket => {
      socket.emit('refresh', {
        ...(NODE_ENV === 'production' ? game : game._doc),
        sessionId: socket.request.session.id
      })
    })

const _notifyDeletedGame = (game, io) => {
  Object.values(io.sockets.sockets)
    .filter(socket =>
      game.players.some(
        player => player.sessionId === socket.request.session.id
      )
    )
    .forEach(socket => {
      socket.emit('deleted')
    })
}

const _upperBonusCheck = scoringMeta =>
  sum(UPPER_BONUS_FIELDS.map(field => scoringMeta[field])) > 62 ? 35 : 0

const _isGameComplete = scores => {
  return every(scores, ({ meta }) => every(meta, v => v !== null))
}

const _isYahtzee = dice =>
  dice.every(({ value }) => value === dice[0].value) && dice.length ? 50 : 0

const _scoreHasYahtzee = score => score.meta.YAHTZEE === 50

module.exports = {
  createGame,
  getSessionGame,
  addPlayerToGame,
  deleteGame,
  removePlayerFromGame,
  startGame,
  rollDice,
  scoreDice
}
