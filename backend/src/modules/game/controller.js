const router = require('express').Router()
const {
  createGame,
  getSessionGame,
  addPlayerToGame,
  deleteGame,
  removePlayerFromGame,
  startGame,
  rollDice,
  scoreDice
} = require('./service')
const { to, handleError, handleSuccess } = require('../../services/apiUtils')
const HttpStatus = require('http-status-codes')

module.exports = (app, io) => {
  app.use('/game', router)

  /**
   * Create a new game, add session user as player
   */
  router.post('/', async (req, res) => {
    const errorMessage = 'Unable to create game'
    const { name } = req.body
    const { data: gameCheckData, error: gameCheckError } = await to(
      getSessionGame(req.session)
    )
    if (gameCheckError) return handleError(res, errorMessage)
    if (gameCheckData)
      return handleError(
        res,
        'Game already exists for player',
        HttpStatus.CONFLICT
      )
    const { data: createdGameData, error: createGameError } = await to(
      createGame(req.session, name)
    )
    if (createGameError) return handleError(res, errorMessage)
    return handleSuccess(res, createdGameData, HttpStatus.CREATED)
  })

  router.patch('/join', async (req, res) => {
    const errorMessage = 'Unable to add player to game'
    const { code, name } = req.body
    const { data: gameCheckData, error: gameCheckError } = await to(
      getSessionGame(req.session)
    )
    if (gameCheckError) return handleError(res, errorMessage)
    if (gameCheckData)
      return handleError(
        res,
        'Game already exists for player',
        HttpStatus.CONFLICT
      )
    const { error: gameError } = await to(
      addPlayerToGame(code, req.session, name, io)
    )
    if (gameError) return handleError(res, errorMessage)
    return handleSuccess(res, null)
  })

  router.patch('/leave', async (req, res) => {
    const errorMessage = 'Unable to leave game'
    const { data: gameCheckData, error: gameCheckError } = await to(
      getSessionGame(req.session)
    )
    if (gameCheckError) return handleError(res, errorMessage)
    if (!gameCheckData)
      return handleError(
        res,
        'No game exists for player',
        HttpStatus.BAD_REQUEST
      )
    const { error: gameError } = await to(removePlayerFromGame(req.session, io))
    if (gameError) return handleError(res, errorMessage)
    return handleSuccess(res, null)
  })

  router.patch('/start', async (req, res) => {
    const errorMessage = 'Unable to start game'
    const { data: gameCheckData, error: gameCheckError } = await to(
      getSessionGame(req.session)
    )
    if (gameCheckError) return handleError(res, errorMessage)
    if (!gameCheckData)
      return handleError(
        res,
        'No game exists for player',
        HttpStatus.BAD_REQUEST
      )
    const { error: gameError } = await to(startGame(req.session, io))
    if (gameError) return handleError(res, errorMessage)
    return handleSuccess(res, null)
  })

  /**
   * Get game for current session
   */
  router.get('/', async (req, res) => {
    const errorMessage = 'Unable to get game'
    const { data, error } = await to(getSessionGame(req.session))
    if (error) return handleError(res, errorMessage)
    return handleSuccess(res, data)
  })

  /**
   * Delete game, only if user is the creator
   */
  router.delete('/', async (req, res) => {
    const { data, error } = await to(deleteGame(req.session, io))
    if (error) return handleError(res, error.message, error.statusCode)
    return handleSuccess(res, data)
  })

  router.patch('/roll', async (req, res) => {
    const { dice } = req.body
    const { data, error } = await to(rollDice(req.session, dice, io))
    if (error) return handleError(res, error.message, error.statusCode)
    return handleSuccess(res, data)
  })

  router.patch('/score', async (req, res) => {
    const { field } = req.body
    const { data, error } = await to(scoreDice(req.session, field, io))
    if (error) return handleError(res, error.message, error.statusCode)
    return handleSuccess(res, data)
  })
}
