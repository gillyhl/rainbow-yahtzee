const { GAME_STATUS, COLORS } = require('./constants')
const diceSchema = {
  value: String,
  color: {
    type: String,
    enum: Object.keys(COLORS)
  }
}

const playerSchema = {
  sessionId: String,
  name: String
}

const scoreMetaSchema = {
  type: Map,
  of: Number
}

const scoresSchema = {
  type: Map,
  of: {
    total: {
      type: Number,
      default: 0
    },
    meta: scoreMetaSchema
  }
}

const gameSchema = {
  code: String,
  players: [playerSchema],
  gameCreator: playerSchema,
  currentPlayer: playerSchema,
  status: {
    type: String,
    enum: Object.values(GAME_STATUS),
    default: GAME_STATUS.WAITING_FOR_PLAYERS
  },
  currentTurn: {
    playerSessionId: String,
    dice: [diceSchema],
    rollsLeft: {
      type: Number,
      default: 3
    }
  },
  scores: scoresSchema
}

module.exports = {
  model: connection => connection.model('Game', gameSchema),
  schema: gameSchema
}
