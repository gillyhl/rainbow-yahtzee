const { DICE, COLORS } = require('../modules/game/constants')

const _getRandomObjectKey = obj => {
  const keys = Object.keys(obj)
  const randomIndex = Math.floor(Math.random() * keys.length)
  return keys[randomIndex]
}

const randomDiceRoll = () => ({
  value: _getRandomObjectKey(DICE),
  color: _getRandomObjectKey(COLORS)
})

module.exports = {
  randomDiceRoll
}
