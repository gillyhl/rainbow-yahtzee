const HttpStatus = require('http-status-codes')
const to = promise =>
  promise.then(data => ({ data })).catch(error => ({ error }))

const handleSuccess = (res, data, statusCode) =>
  res.status(statusCode || HttpStatus.OK).json({
    data,
    success: true,
    statusCode
  })

const handleError = (res, message = 'Internal Server Error', statusCode) =>
  res.status(statusCode || HttpStatus.INTERNAL_SERVER_ERROR).json({
    data: { message },
    success: false,
    statusCode
  })

module.exports = {
  to,
  handleSuccess,
  handleError
}
